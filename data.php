<?php
require_once 'config.php';
try {
    $pdo = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
}catch(PDOException $e){
    echo "Невозможно установить соединение с БД!";
}


//расписание занятий для конкретной группы

$query="SELECT
raspisanie.id_zan,
gr.ng,
dolzhnosti.d,
teachers.surname,
teachers.`name`,
teachers.middlename,
disc.nd,
vid_zanyatii.vz,
camps.adress,
lecture_rooms.num_lr,
raspisanie.date_zan,
raspisanie.time_start,
raspisanie.count_hours
FROM
camps
INNER JOIN lecture_rooms ON lecture_rooms.id_camp = camps.id_camp
INNER JOIN raspisanie ON raspisanie.id_lr = lecture_rooms.id_lr
INNER JOIN teacher_gr ON raspisanie.id_tg = teacher_gr.id_tg
INNER JOIN chasi_disc ON teacher_gr.id_cd = chasi_disc.id_cd
INNER JOIN vid_zanyatii ON chasi_disc.id_vz = vid_zanyatii.id_vz
INNER JOIN teachers ON teacher_gr.id_t = teachers.id_t
INNER JOIN dolzhnosti ON teachers.id_d = dolzhnosti.id_d
INNER JOIN gr ON teacher_gr.id_gr = gr.id_gr
INNER JOIN ucheb_pl ON gr.id_plan = ucheb_pl.id_plan
INNER JOIN form_study ON ucheb_pl.id_fo = form_study.id_fo
INNER JOIN specs ON ucheb_pl.id_spec = specs.id_spec
INNER JOIN disc_spec ON disc_spec.id_plan = ucheb_pl.id_plan AND chasi_disc.id_ds = disc_spec.id_ds
INNER JOIN disc ON disc_spec.id_disc = disc.id_disc
ORDER BY
gr.ng ASC";



$result=$pdo->query($query);



//расписание занятий преподавателя








$query2="SELECT
raspisanie.id_zan,
gr.ng,
dolzhnosti.d,
teachers.surname,
teachers.`name`,
teachers.middlename,
disc.nd,
vid_zanyatii.vz,
camps.adress,
lecture_rooms.num_lr,
raspisanie.date_zan,
raspisanie.time_start,
raspisanie.count_hours
FROM
camps
INNER JOIN lecture_rooms ON lecture_rooms.id_camp = camps.id_camp
INNER JOIN raspisanie ON raspisanie.id_lr = lecture_rooms.id_lr
INNER JOIN teacher_gr ON raspisanie.id_tg = teacher_gr.id_tg
INNER JOIN chasi_disc ON teacher_gr.id_cd = chasi_disc.id_cd
INNER JOIN vid_zanyatii ON chasi_disc.id_vz = vid_zanyatii.id_vz
INNER JOIN teachers ON teacher_gr.id_t = teachers.id_t
INNER JOIN dolzhnosti ON teachers.id_d = dolzhnosti.id_d
INNER JOIN gr ON teacher_gr.id_gr = gr.id_gr
INNER JOIN ucheb_pl ON gr.id_plan = ucheb_pl.id_plan
INNER JOIN form_study ON ucheb_pl.id_fo = form_study.id_fo
INNER JOIN specs ON ucheb_pl.id_spec = specs.id_spec
INNER JOIN disc_spec ON disc_spec.id_plan = ucheb_pl.id_plan AND chasi_disc.id_ds = disc_spec.id_ds
INNER JOIN disc ON disc_spec.id_disc = disc.id_disc
ORDER BY
teachers.surname ASC
";


$result2=$pdo->query($query2);









//сведения о проведенных преподавателем занятиях








$query3="SELECT
raspisanie.id_zan,
gr.ng,
dolzhnosti.d,
teachers.surname,
teachers.`name`,
teachers.middlename,
disc.nd,
vid_zanyatii.vz,
camps.adress,
lecture_rooms.num_lr,
raspisanie.date_zan,
raspisanie.time_start,
raspisanie.count_hours
FROM
camps
INNER JOIN lecture_rooms ON lecture_rooms.id_camp = camps.id_camp
INNER JOIN raspisanie ON raspisanie.id_lr = lecture_rooms.id_lr
INNER JOIN teacher_gr ON raspisanie.id_tg = teacher_gr.id_tg
INNER JOIN chasi_disc ON teacher_gr.id_cd = chasi_disc.id_cd
INNER JOIN vid_zanyatii ON chasi_disc.id_vz = vid_zanyatii.id_vz
INNER JOIN teachers ON teacher_gr.id_t = teachers.id_t
INNER JOIN dolzhnosti ON teachers.id_d = dolzhnosti.id_d
INNER JOIN gr ON teacher_gr.id_gr = gr.id_gr
INNER JOIN ucheb_pl ON gr.id_plan = ucheb_pl.id_plan
INNER JOIN form_study ON ucheb_pl.id_fo = form_study.id_fo
INNER JOIN specs ON ucheb_pl.id_spec = specs.id_spec
INNER JOIN disc_spec ON disc_spec.id_plan = ucheb_pl.id_plan AND chasi_disc.id_ds = disc_spec.id_ds
INNER JOIN disc ON disc_spec.id_disc = disc.id_disc
WHERE
teachers.surname = 'Васечкин' AND
raspisanie.date_zan < CURRENT_DATE";


$result3=$pdo->query($query3);



//поиск свободных аудиторий в заданное время



$query4="SELECT
lecture_rooms.num_lr
FROM
camps
INNER JOIN lecture_rooms ON lecture_rooms.id_camp = camps.id_camp
INNER JOIN raspisanie ON raspisanie.id_lr = lecture_rooms.id_lr
INNER JOIN teacher_gr ON raspisanie.id_tg = teacher_gr.id_tg
INNER JOIN chasi_disc ON teacher_gr.id_cd = chasi_disc.id_cd
INNER JOIN vid_zanyatii ON chasi_disc.id_vz = vid_zanyatii.id_vz
INNER JOIN teachers ON teacher_gr.id_t = teachers.id_t
INNER JOIN dolzhnosti ON teachers.id_d = dolzhnosti.id_d
INNER JOIN gr ON teacher_gr.id_gr = gr.id_gr
INNER JOIN ucheb_pl ON gr.id_plan = ucheb_pl.id_plan
INNER JOIN form_study ON ucheb_pl.id_fo = form_study.id_fo
INNER JOIN specs ON ucheb_pl.id_spec = specs.id_spec
INNER JOIN disc_spec ON disc_spec.id_plan = ucheb_pl.id_plan AND chasi_disc.id_ds = disc_spec.id_ds
INNER JOIN disc ON disc_spec.id_disc = disc.id_disc
WHERE
CURRENT_TIME NOT BETWEEN raspisanie.time_start AND ADDDATE(raspisanie.time_start,INTERVAL 3 HOUR)
";
$result4=$pdo->query($query4);






//информация об общем количестве часов по
//дисциплинам по видам занятий


$query5 = "SELECT
disc.nd,
Sum(chasi_disc.chasi)
FROM
disc
INNER JOIN disc_spec ON disc_spec.id_disc = disc.id_disc
INNER JOIN chasi_disc ON chasi_disc.id_ds = disc_spec.id_ds
INNER JOIN vid_zanyatii ON chasi_disc.id_vz = vid_zanyatii.id_vz
GROUP BY
disc.nd
";

$result5=$pdo->query($query5);

//информация об общем количестве часов по видам занятий


$query6 = "SELECT
vid_zanyatii.vz,
Sum(chasi_disc.chasi)
FROM
disc
INNER JOIN disc_spec ON disc_spec.id_disc = disc.id_disc
INNER JOIN chasi_disc ON chasi_disc.id_ds = disc_spec.id_ds
INNER JOIN vid_zanyatii ON chasi_disc.id_vz = vid_zanyatii.id_vz
GROUP BY
vid_zanyatii.vz
";

$result6=$pdo->query($query6);



//наличие «форточек» у студентов и
//преподавателей




$query7 = "SELECT
raspisanie.id_zan,
raspisanie_1.id_zan,
raspisanie.date_zan,
raspisanie.time_start,
raspisanie_1.time_start,
raspisanie.id_lr,
teacher_gr.id_t
FROM
raspisanie
INNER JOIN raspisanie AS raspisanie_1 ON raspisanie.date_zan = raspisanie_1.date_zan AND raspisanie.time_start < raspisanie_1.time_start
INNER JOIN teacher_gr ON raspisanie.id_tg = teacher_gr.id_tg
INNER JOIN teacher_gr AS teacher_gr_1 ON raspisanie_1.id_tg = teacher_gr_1.id_tg AND teacher_gr.id_t = teacher_gr_1.id_t
WHERE
ADDDATE(raspisanie.time_start,INTERVAL 3 HOUR) < raspisanie_1.time_start
ORDER BY
teacher_gr.id_t ASC,
raspisanie.date_zan ASC,
raspisanie.time_start ASC,
raspisanie_1.time_start ASC
";
$result7=$pdo->query($query7);


