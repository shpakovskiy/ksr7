<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php require_once 'data.php'; ?>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>ksr7</title>
</head>
<body>



<?php //var_dump($result->fetch());?>

<h1>Запрос на расписание,сортировка по полю "ng"</h1>
<h4>расписание занятий для конкретной группы</h4>
<h4>для большей конкретности можно дописать WHERE в запросе...</h4>


<br>
<br>


<table class="table table-dark">

    <thead>


    <tr>

        <?php foreach($result->fetch(PDO::FETCH_ASSOC) as $k=>$v){?>
        <th scope="col"><?php echo $k; ?></th>

        <?php }?>
    </tr>


    </thead>

    <tbody>
    <?php while ($row=$result->fetch(PDO::FETCH_ASSOC)){ ?>
    <tr>
        <?php foreach($row as $v){?>
        <td><?php echo $v; ?></td>
        <?php }?>


    </tr>
<?php }?>

    </tbody>
</table>

<br>
<br>
<br>
<br>


<h1>Запрос на расписание,сортировка по полю "surname"</h1>
<h4>расписание занятий преподавателя</h4>
<h4>для большей конкретности можно дописать WHERE в запросе...</h4>



<br>
<br>



<table class="table table-dark">

    <thead>


    <tr>

        <?php foreach($result2->fetch(PDO::FETCH_ASSOC) as $k=>$v){?>
            <th scope="col"><?php echo $k; ?></th>

        <?php }?>
    </tr>


    </thead>

    <tbody>
    <?php while ($row=$result2->fetch(PDO::FETCH_ASSOC)){ ?>
        <tr>
            <?php foreach($row as $v){?>
                <td><?php echo $v; ?></td>
            <?php }?>


        </tr>
    <?php }?>

    </tbody>
</table>


<br>
<br>
<br>
<br>






<h1>Сведения о проведенных преподавателем занятиях</h1>
<h4>на примере Васечкина</h4>



<br>
<br>



<table class="table table-dark">

    <thead>


    <tr>

        <?php foreach($result3->fetch(PDO::FETCH_ASSOC) as $k=>$v){?>
            <th scope="col"><?php echo $k; ?></th>

        <?php }?>
    </tr>


    </thead>

    <tbody>
    <?php while ($row=$result3->fetch(PDO::FETCH_ASSOC)){ ?>
        <tr>
            <?php foreach($row as $v){?>
                <td><?php echo $v; ?></td>
            <?php }?>


        </tr>
    <?php }?>

    </tbody>
</table>


<br>
<br>
<br>
<br>




<h1>Поиск свободных аудиторий в заданное время</h1>
<h4>Это свободные аудитории.Время задано в запросе.</h4>



<br>
<br>



<table class="table table-dark">

    <thead>


    <tr>

        <?php foreach($result4->fetch(PDO::FETCH_ASSOC) as $k=>$v){?>
            <th scope="col"><?php echo $k; ?></th>

        <?php }?>
    </tr>


    </thead>

    <tbody>
    <?php while ($row=$result4->fetch(PDO::FETCH_ASSOC)){ ?>
        <tr>
            <?php foreach($row as $v){?>
                <td><?php echo $v; ?></td>
            <?php }?>


        </tr>
    <?php }?>

    </tbody>
</table>


<br>
<br>
<br>
<br>


<h1>Информация об общем количестве часов по
    дисциплинам </h1>



<br>
<br>



<table class="table table-dark">

    <thead>


    <tr>

        <?php foreach($result5->fetch(PDO::FETCH_ASSOC) as $k=>$v){?>
            <th scope="col"><?php echo $k; ?></th>

        <?php }?>
    </tr>


    </thead>

    <tbody>
    <?php while ($row=$result5->fetch(PDO::FETCH_ASSOC)){ ?>
        <tr>
            <?php foreach($row as $v){?>
                <td><?php echo $v; ?></td>
            <?php }?>


        </tr>
    <?php }?>

    </tbody>
</table>


<br>
<br>
<br>
<br>





<h1>Информация об общем количестве часов по
    видам занятий </h1>



<br>
<br>



<table class="table table-dark">

    <thead>


    <tr>

        <?php foreach($result6->fetch(PDO::FETCH_ASSOC) as $k=>$v){?>
            <th scope="col"><?php echo $k; ?></th>

        <?php }?>
    </tr>


    </thead>

    <tbody>
    <?php while ($row=$result6->fetch(PDO::FETCH_ASSOC)){ ?>
        <tr>
            <?php foreach($row as $v){?>
                <td><?php echo $v; ?></td>
            <?php }?>


        </tr>
    <?php }?>

    </tbody>
</table>


<br>
<br>
<br>
<br>




<h1>Наличие «форточек» у студентов и
    преподавателей </h1>



<br>
<br>



<table class="table table-dark">

    <thead>


    <tr>

        <?php foreach($result7->fetch(PDO::FETCH_ASSOC) as $k=>$v){?>
            <th scope="col"><?php echo $k; ?></th>

        <?php }?>
    </tr>


    </thead>

    <tbody>
    <?php while ($row=$result7->fetch(PDO::FETCH_ASSOC)){ ?>
        <tr>
            <?php foreach($row as $v){?>
                <td><?php echo $v; ?></td>
            <?php }?>


        </tr>
    <?php }?>

    </tbody>
</table>


<br>
<br>
<br>
<br>
















<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>